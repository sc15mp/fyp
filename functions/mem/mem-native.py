#!/usr/bin/env python3
import sys
from random import randint
from itertools import starmap
from operator import mul

def main(args):

    n = int(args[1])

    for i in range (n):

        a = [[randint(1, 10) for i in range(10)] for j in range(10)]
        b = [[randint(1, 10) for i in range(10)] for j in range(10)]
        ab = [[sum(starmap(mul, zip(row, col))) for col in zip(*b)] for row in a]

    print("Value for N: " + str(n))


if __name__ == "__main__":
    main(sys.argv)

