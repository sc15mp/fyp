from random import randint
from itertools import starmap
from operator import mul

def main(args):

    n = args['n']

    for i in range (n):
        a = [[randint(1, 10) for i in range(10)] for j in range(10)]
        b = [[randint(1, 10) for i in range(10)] for j in range(10)]
        ab = [[sum(starmap(mul, zip(row, col))) for col in zip(*b)] for row in a]

    return {"Value for N": n}
