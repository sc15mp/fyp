#!/usr/bin/env python3
import sys

from decimal import *

getcontext().prec = 50

def main(args):

    n = int(args[1])

    pi = Decimal(sum((Decimal(1)/(16**k))*((Decimal(4)/(8*k+1))-(Decimal(2)/(8*k+4))-(Decimal(1)/(8*k+5))-(Decimal(1)/(8*k+6))) for k in range(n)))

    print("Value for N: " + str(n))

if __name__ == "__main__":
    main(sys.argv)

