from decimal import *

getcontext().prec = 50

def main(args):

    n = args['n']

    pi = Decimal(sum((Decimal(1)/(16**k))*((Decimal(4)/(8*k+1))-(Decimal(2)/(8*k+4))-(Decimal(1)/(8*k+5))-(Decimal(1)/(8*k+6))) for k in range(n)))

    return {"Value for N": n}
