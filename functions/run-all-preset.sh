#!/bin/bash

tasks=(
  "whisk  cpu 16 10000"
  "docker cpu 16 10000"
  "native cpu 16 10000"
  "whisk  mem 16 50000"
  "docker mem 16 50000"
  "native mem 16 50000"
  "whisk  net 16 500"
  "docker net 16 500"
  "native net 16 500"
)

for task in "${tasks[@]}"; do

  for ((it = 1; it <= 5; it++)); do

    eval ~/functions/run-single.sh "$task"
    sleep 1m

  done

done
