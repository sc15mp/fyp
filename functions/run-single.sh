#!/bin/bash

command=""

if [ "$1" == "whisk" ]; then
  command="wsk -i action invoke $2 --blocking --param n"
elif [ "$1" == "docker" ]; then
  command="docker run --rm $2-docker"
elif [ "$1" == "native" ]; then
  command="python3 ~/functions/$2/$2-native.py"
fi

step=$(($4/5))

printf "$1\n%s\n" $(printf '%.s=' {1..40}) | tee -a ~/functions/output/$1-$2-$3-$4.txt

for ((value = $step; value <= $4; value += $step)); do

  printf "N = $value\n%s\n" $(printf '%.s-' {1..20}) | tee -a ~/functions/output/$1-$2-$3-$4.txt

  for ((it = 1; it <= $3; it++)); do
    eval /usr/bin/time -f "%e" $command $value 2>&1 > /dev/null | tee -a ~/functions/output/$1-$2-$3-$4.txt &
  done

  wait
  sleep 20s

  echo "" | tee -a ~/functions/output/$1-$2-$3-$4.txt

done
